const express = require("express");
const app = express();
const port = 8080;
const mock = require("./main.js");

const mockConfig = [
    { url: "/api/", dir: "test/api" },
    { url: "/api2/", dir: "test/api" },
    { url: "/apiX/", dir: "test/apiX", delay: 1000 }
];

mock.config(mockConfig);

app.use("*", (req, res, next) => {
    mock.mockRequest(req, res, next);
});

var server = app.listen(port, function() {
    console.log("Example app listening at port", port);
});

module.exports = server;
