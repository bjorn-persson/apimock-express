var chai = require("chai");
var expect = chai.expect;
var request = require("request");

describe("Intercepted call", function() {
    var server;

    before(function() {
        server = require("./../../test-server");
    });

    it("Should handle when an earlier middleware intercepts request body", function(done) {
        var requestbody = { foo: "bar" };
        request.get(
            "http://localhost:8080/api/intercepted",
            { json: requestbody },
            function(err, res, body) {
                expect(res.statusCode).to.equal(200);
                expect(body).to.deep.equal({ id: "api" });
                done();
            }
        );
    });
});
