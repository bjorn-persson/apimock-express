var chai = require("chai");
var expect = chai.expect;
var fs = require("fs");
var request = require("request");

describe("Url matching", function() {
    var server;

    before(function() {
        server = require("./../../test-server");
    });

    it("A correct url that starts with mock url should match", function(done) {
        var expectedBody = fs.readFileSync("test/api/simple/users.json", {
            encoding: "utf-8"
        });
        request.get("http://localhost:8080/api/simple/users/", function(
            err,
            res,
            body
        ) {
            expect(res.statusCode).to.equal(200);
            expect(body).to.equal(expectedBody);
            done();
        });
    });

    it("Must start with the mock url", function(done) {
        //The url matches but not in the beginning
        var expectedBody = "Cannot GET /foo/api/simple/users/";
        request.get("http://localhost:8080/foo/api/simple/users/", function(
            err,
            res,
            body
        ) {
            expect(res.statusCode).to.equal(404);
            expect(body).to.contain(expectedBody);
            done();
        });
    });

    it("A incorrect url should not match", function(done) {
        var expectedBody = "Cannot GET /foo/simple/users/";
        request.get("http://localhost:8080/foo/simple/users/", function(
            err,
            res,
            body
        ) {
            expect(res.statusCode).to.equal(404);
            expect(body).to.contain(expectedBody);
            done();
        });
    });
});
