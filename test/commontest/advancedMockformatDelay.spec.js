var chai = require("chai");
var expect = chai.expect;
var request = require("request");

describe("Advanced mockformat", function() {
    var server;

    before(function() {
        server = require("./../../test-server");
    });

    var DELAY_TIME = 1000;

    describe("Delay", function() {
        it("GET /api/advanced/delay?foo=bar should not be delayed", function(done) {
            var starttime = new Date();
            request.get(
                "http://localhost:8080/api/advanced/delay?foo=bar",
                function(err, res, body) {
                    var endtime = new Date();
                    var executionTime = endtime.getTime() - starttime.getTime();
                    expect(res.statusCode).to.equal(200);
                    expect(body).to.equal('{"message":"default"}');
                    expect(executionTime).to.be.at.most(DELAY_TIME);
                    done();
                }
            );
        });

        it("GET /api/advanced/delay?foo=foo should be delayed", function(done) {
            var starttime = new Date();
            request.get(
                "http://localhost:8080/api/advanced/delay?foo=foo",
                function(err, res, body) {
                    var endtime = new Date();
                    var executionTime = endtime.getTime() - starttime.getTime();
                    expect(res.statusCode).to.equal(200);
                    expect(body).to.equal('{"message":"delayed"}');
                    expect(executionTime).to.be.at.least(DELAY_TIME);
                    done();
                }
            );
        });
    });
});
