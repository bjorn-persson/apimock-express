var chai = require("chai");
var expect = chai.expect;
var request = require("request");

describe("Examplefile", function() {
    var server;

    before(function() {
        server = require("./../../test-server");
    });

    it("Nothing matches", function(done) {
        var requestbody = {};
        request.post(
            "http://localhost:8080/api/examplefile",
            { json: requestbody },
            function(err, res, body) {
                expect(res.statusCode).to.equal(201);
                expect(body).to.deep.equal({
                    message: "Nothing matches. Default response"
                });
                done();
            }
        );
    });

    it("One request parameter matches", function(done) {
        var requestbody = {};
        request.post(
            "http://localhost:8080/api/examplefile?foo=bar",
            { json: requestbody },
            function(err, res, body) {
                expect(res.statusCode).to.equal(402);
                expect(body).to.deep.equal({
                    message: "One parameter matches"
                });
                done();
            }
        );
    });

    it("Two request parameters matches", function(done) {
        var requestbody = {};
        request.post(
            "http://localhost:8080/api/examplefile?foo=bar&bar=foo",
            { json: requestbody },
            function(err, res, body) {
                expect(res.statusCode).to.equal(401);
                expect(body).to.deep.equal({
                    message: "Two parameters matches"
                });
                done();
            }
        );
    });

    it("One body parameter matches", function(done) {
        var requestbody = { foo: "foo" };
        request.post(
            "http://localhost:8080/api/examplefile",
            { json: requestbody },
            function(err, res, body) {
                expect(res.statusCode).to.equal(404);
                expect(body).to.deep.equal({
                    message: "One body parameter matches"
                });
                done();
            }
        );
    });

    it("Two body parameters matches", function(done) {
        var requestbody = {
            user: { firstname: "Luke", lastname: "Skywalker" }
        };
        request.post(
            "http://localhost:8080/api/examplefile",
            { json: requestbody },
            function(err, res, body) {
                expect(res.statusCode).to.equal(403);
                expect(body).to.deep.equal({
                    message: "Two body parameters matches"
                });
                done();
            }
        );
    });

    it("Both request parameter and body matches", function(done) {
        var requestbody = { bar: "foo" };
        request.post(
            "http://localhost:8080/api/examplefile?foo=bar",
            { json: requestbody },
            function(err, res, body) {
                expect(res.statusCode).to.equal(400);
                expect(body).to.deep.equal({
                    message: "Both parameter and body matches"
                });
                done();
            }
        );
    });

    it("One body parameter matches. Default status", function(done) {
        var requestbody = { foo: "bar" };
        request.post(
            "http://localhost:8080/api/examplefile",
            { json: requestbody },
            function(err, res, body) {
                expect(res.statusCode).to.equal(200);
                expect(body).to.deep.equal({
                    message: "One body parameter matches. Default status"
                });
                done();
            }
        );
    });
});
