var chai = require("chai");
var expect = chai.expect;
var fs = require("fs");
var request = require("request");

describe("Simple mockformat", function() {
    var server;

    before(function() {
        server = require("./../../test-server");
    });

    it("GET /api/simple/users/ should return test/api/simple/users.json", function(done) {
        var expectedBody = fs.readFileSync("test/api/simple/users.json", {
            encoding: "utf-8"
        });
        request.get("http://localhost:8080/api/simple/users/", function(
            err,
            res,
            body
        ) {
            expect(res.statusCode).to.equal(200);
            expect(res.headers["content-type"]).to.equal(
                "application/json;charset=UTF-8"
            );
            expect(body).to.equal(expectedBody);
            done();
        });
    });

    it("GET /api/simple/users/1 should return test/api/simple/users/1.json", function(done) {
        var expectedBody = fs.readFileSync("test/api/simple/users/1.json", {
            encoding: "utf-8"
        });
        request.get("http://localhost:8080/api/simple/users/1", function(
            err,
            res,
            body
        ) {
            expect(res.statusCode).to.equal(200);
            expect(res.headers["content-type"]).to.equal(
                "application/json;charset=UTF-8"
            );
            expect(body).to.equal(expectedBody);
            done();
        });
    });

    it("POST /api/simple/users/ should return test/api/simple/users_post.json", function(done) {
        var expectedBody = fs.readFileSync("test/api/simple/users_post.json", {
            encoding: "utf-8"
        });
        request.post("http://localhost:8080/api/simple/users/", function(
            err,
            res,
            body
        ) {
            expect(res.statusCode).to.equal(200);
            expect(res.headers["content-type"]).to.equal(
                "application/json;charset=UTF-8"
            );
            expect(body).to.equal(expectedBody);
            done();
        });
    });

    it("PUT /api/simple/users/ should return test/api/simple/users/1_put.json", function(done) {
        var expectedBody = fs.readFileSync("test/api/simple/users/1_put.json", {
            encoding: "utf-8"
        });
        request.put("http://localhost:8080/api/simple/users/1", function(
            err,
            res,
            body
        ) {
            expect(res.statusCode).to.equal(200);
            expect(res.headers["content-type"]).to.equal(
                "application/json;charset=UTF-8"
            );
            expect(body).to.equal(expectedBody);
            done();
        });
    });

    it("DELETE /api/simple/users/ should return test/api/simple/users/1_delete.json", function(done) {
        var expectedBody = fs.readFileSync(
            "test/api/simple/users/1_delete.json",
            { encoding: "utf-8" }
        );
        request.delete("http://localhost:8080/api/simple/users/1", function(
            err,
            res,
            body
        ) {
            expect(res.statusCode).to.equal(200);
            expect(res.headers["content-type"]).to.equal(
                "application/json;charset=UTF-8"
            );
            expect(body).to.equal(expectedBody);
            done();
        });
    });

    it("path not found should return an error", function(done) {
        var expectedBody = { error: "Can not find mockfile. See server log" };
        request.get("http://localhost:8080/api/simple/users/1234", function(
            err,
            res,
            body
        ) {
            expect(res.statusCode).to.equal(404);
            expect(res.headers["content-type"]).to.equal(
                "application/json;charset=UTF-8"
            );
            expect(JSON.parse(body)).to.deep.equal(expectedBody);
            done();
        });
    });
});
