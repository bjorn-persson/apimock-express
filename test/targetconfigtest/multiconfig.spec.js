var chai = require("chai");
var expect = chai.expect;
var fs = require("fs");
var request = require("request");

describe("Multiconfig", function() {
    var server;

    before(function() {
        server = require("./../../test-server");
    });

    it("Local config GET /api2/simple/users/ should return test/api/simple/users.json", function(done) {
        var expectedBody = fs.readFileSync("test/api/simple/users.json", {
            encoding: "utf-8"
        });
        request.get("http://localhost:8080/api2/simple/users/", function(
            err,
            res,
            body
        ) {
            expect(res.statusCode).to.equal(200);
            expect(body).to.equal(expectedBody);
            done();
        });
    });

    it("Multiple config GET /api2/hello should return test/api/hello.json", function(done) {
        var expectedBody = '{"id":"api"}';
        request.get("http://localhost:8080/api2/hello", function(
            err,
            res,
            body
        ) {
            expect(res.statusCode).to.equal(200);
            expect(body).to.equal(expectedBody);
            done();
        });
    });

    it("Multiple config GET /apiX/hello should return test/apiX/hello.json", function(done) {
        var expectedBody = '{"id":"apiX"}';
        request.get("http://localhost:8080/apiX/hello", function(
            err,
            res,
            body
        ) {
            expect(res.statusCode).to.equal(200);
            expect(body).to.equal(expectedBody);
            done();
        });
    });
});
