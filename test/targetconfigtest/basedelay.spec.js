var chai = require("chai");
var expect = chai.expect;
var request = require("request");

describe("Basedelay", function() {
    var DELAY_TIME = 1000;

    var server;

    before(function() {
        server = require("./../../test-server");
    });

    describe("simple mockformat", function() {
        it("GET /api2/hello should not be delayed", function(done) {
            var starttime = new Date();
            var expectedBody = '{"id":"api"}';
            request.get("http://localhost:8080/api2/hello", function(
                err,
                res,
                body
            ) {
                var endtime = new Date();
                var executionTime = endtime.getTime() - starttime.getTime();
                expect(res.statusCode).to.equal(200);
                expect(body).to.equal(expectedBody);
                expect(executionTime).to.be.at.most(DELAY_TIME);
                done();
            });
        });

        it("GET /apiX/hello should be delayed", function(done) {
            var starttime = new Date();
            var expectedBody = '{"id":"apiX"}';
            request.get("http://localhost:8080/apiX/hello", function(
                err,
                res,
                body
            ) {
                var endtime = new Date();
                var executionTime = endtime.getTime() - starttime.getTime();
                expect(res.statusCode).to.equal(200);
                expect(body).to.equal(expectedBody);
                expect(executionTime).to.be.at.least(DELAY_TIME);
                done();
            });
        });
    });

    describe("advanced mockformat", function() {
        it("GET /api2/helloAdv should not be delayed", function(done) {
            var starttime = new Date();
            var expectedBody = '{"id":"api"}';
            request.get("http://localhost:8080/api2/helloAdv", function(
                err,
                res,
                body
            ) {
                var endtime = new Date();
                var executionTime = endtime.getTime() - starttime.getTime();
                expect(res.statusCode).to.equal(200);
                expect(body).to.equal(expectedBody);
                expect(executionTime).to.be.at.most(DELAY_TIME);
                done();
            });
        });

        it("GET /apiX/helloAdv should be delayed", function(done) {
            var starttime = new Date();
            var expectedBody = '{"id":"apiX"}';
            request.get("http://localhost:8080/apiX/helloAdv", function(
                err,
                res,
                body
            ) {
                var endtime = new Date();
                var executionTime = endtime.getTime() - starttime.getTime();
                expect(res.statusCode).to.equal(200);
                expect(body).to.equal(expectedBody);
                expect(executionTime).to.be.at.least(DELAY_TIME);
                done();
            });
        });
    });
});
