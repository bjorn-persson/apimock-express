var fs = require("fs");
var path = require("path");
var url = require("url");
var util = require("util");
var glob = require("glob");

var mockOptions = [];
var defaultStatus = 200;
var defaultContentType = "application/json;charset=UTF-8";

module.exports = {
    /**
     * Configuration of the mock.
     */
    config: function(options) {
        var optionArray = util.isArray(options) ? options : [options];
        optionArray.forEach(function(option) {
            var mockOption = {};
            mockOption.mockurl = option.url;
            mockOption.mockdir = path.join(process.cwd(), option.dir);
            mockOption.delay = option.delay;
            console.log("Mocking api calls from: " + mockOption.mockurl);
            console.log("to: " + mockOption.mockdir);
            if (mockOption.delay !== undefined) {
                console.log(
                    "With delay of " +
                        (mockOption.delay > 0 ? mockOption.delay : 1) +
                        " milliseconds"
                );
            }
            mockOptions.push(mockOption);
        });
    },

    /**
     * The Connect middleware function that handles a request.
     */
    mockRequest: function(req, res, next) {
        var optionIndex = -1;
        mockOptions.forEach(function(mockOption, index) {
            if (req.originalUrl.indexOf(mockOption.mockurl) === 0) {
                //starts with mockurl
                optionIndex = index;
            }
        });
        if (optionIndex < 0) {
            //req.originalUrl matches no mockurl
            next();
        } else {
            //req.originalUrl matches some mockurl
            res.setHeader("Access-Control-Allow-Origin", "*");
            res.setHeader(
                "Access-Control-Allow-Methods",
                "GET,PUT,POST,DELETE"
            );
            res.setHeader("Access-Control-Allow-Headers", "Content-Type");

            var filepath = createFilepath(req, optionIndex);
            if (fs.existsSync(filepath)) {
                var fileContent = extractFileContent(filepath);

                if (fileContent.length === 0) {
                    //Empty file
                    res.writeHead(defaultStatus, {
                        "Content-Type": defaultContentType
                    });
                    res.end();
                } else {
                    //Respond with the mockfile data
                    var baseDelay = parseDelay(mockOptions[optionIndex].delay);
                    respondWithMock(req, res, fileContent, filepath, baseDelay);
                }
            } else {
                console.error("Can not find " + filepath);
                var response = {
                    status: 404,
                    body: { error: "Can not find mockfile. See server log" }
                };
                respondData(res, response);
            }
        }
    }
};

/**
 * Extracts filecontent for both js and json files
 */
function extractFileContent(filepath) {
    switch (path.extname(filepath)) {
        case ".json":
            return fs.readFileSync(filepath, { encoding: "utf8" });
            break;
        case ".js":
            return require(filepath);
            break;
    }
}

/**
 * Create the path to the mockfile depending on the request url and the http method.
 */
function createFilepath(req, optionIndex) {
    var filepath = req.originalUrl;
    //remove mockurl beginning
    filepath = filepath.substring(mockOptions[optionIndex].mockurl.length);
    //remove trailing /
    if (filepath.indexOf("/", filepath.length - 1) !== -1) {
        filepath = filepath.substring(0, filepath.length - 1);
    }
    //remove parameters
    var questionMarkPos = filepath.indexOf("?");
    if (questionMarkPos !== -1) {
        filepath = filepath.substring(0, questionMarkPos);
    }
    //add method if not GET
    if (req.method !== "GET") {
        filepath = filepath + "_" + req.method.toLowerCase();
        //config.method = 'GET';
    }

    filepath = path.join(mockOptions[optionIndex].mockdir, filepath);

    // add file extension
    const files = glob.sync(filepath + ".*");

    return files[0];
}

/**
 * Make sure that delay is a number or return a 0
 */
function parseDelay(delay) {
    if (delay === undefined) {
        return 0;
    }
    if (!isNaN(parseFloat(delay)) && isFinite(delay)) {
        //delay is a number
        return delay;
    } else {
        return 0;
    }
}

/**
 * Respond the mockfile data to the client
 */
function respondWithMock(req, res, fileContent, filepath, baseDelay) {
    var mockdata;
    try {
        mockdata = JSON.parse(fileContent);
    } catch (err) {
        console.error("Malformed file: " + filepath);
        mockdata = {
            defaultResponse: {
                status: 500,
                body: { error: "Malformed mockfile. See server log" }
            }
        };
    }
    if (
        mockdata.responses === undefined &&
        mockdata.defaultResponse === undefined
    ) {
        //The mockfile has the simple format. Just respond with the mockfile content.
        setTimeout(simpleMockformat, baseDelay, res, filepath);
    } else {
        //The mockfile has the advanced format.
        //Parse the mockfile and respond with the selected response depending on the request.
        advancedMockformat(req, res, mockdata, baseDelay);
    }
}

/**
 * Respond with the mockfile content and the default status
 */
function simpleMockformat(res, filepath) {
    res.writeHead(defaultStatus, { "Content-Type": defaultContentType });
    var filestream = fs.createReadStream(filepath);
    filestream.pipe(res);
}

/**
 * Parse the mockfile and respond with the selected response depending on the request
 */
function advancedMockformat(req, res, mockdata, baseDelay) {
    var requestParameters = url.parse(req.originalUrl, true).query;
    var cookies = parseCookies(req);
    var bodyParameters = {};
    var body = "";
    req.on("data", function(chunk) {
        body += chunk;
    });
    req.on("end", function() {
        var parseError = false;
        try {
            bodyParameters = parseBody(req, body);
        } catch (err) {
            parseError = true;
        }
        var selectedResponse;
        if (parseError) {
            console.error("Malformed input body. url: " + req.originalUrl);
            selectedResponse = {
                status: 500,
                body: { error: "Malformed input body" }
            };
        } else {
            selectedResponse = selectResponse(
                res,
                mockdata,
                requestParameters,
                bodyParameters,
                req.headers,
                cookies
            );
        }
        var requestDelay =
            selectedResponse !== undefined
                ? parseDelay(selectedResponse.delay)
                : 0;
        var delay = requestDelay + baseDelay;
        setTimeout(respondData, delay, res, selectedResponse);
    });
}

/**
 * Parse the request cookies into a js object
 */
function parseCookies(request) {
    var cookies = {};
    if (request.headers.cookie) {
        request.headers.cookie.split(";").forEach(function(cookie) {
            var parts = cookie.split("=");
            cookies[parts[0].trim()] = (parts[1] || "").trim();
        });
    }
    return cookies;
}

/**
 * If the request Content-Type is json.
 * Then parse the json body into a js object.
 * Or return an empty object.
 */
function parseBody(req, body) {
    var bodyParameters = {};
    var contentType = req.headers["content-type"];
    if (contentType === undefined || body.length === 0) {
        //No Content-Type or no body. Don't parse the body
        return bodyParameters;
    }
    if (contentType.includes("application/json")) {
        //Content-Type is json
        bodyParameters = JSON.parse(body);
    }
    return bodyParameters;
}

/**
 * Write the selected response to the client.
 */
function respondData(res, response) {
    if (response) {
        var status = defaultStatus;
        if (response.status) {
            status = response.status;
        }
        var body = "";
        if (response.body) {
            body = JSON.stringify(response.body);
        }
        res.writeHead(status, { "Content-Type": defaultContentType });
        res.write(body);
        res.end();
    } else {
        console.error("No response could be found");
        res.writeHead(500, { "Content-Type": defaultContentType });
        res.write('{"error":"No response could be found"}');
        res.end();
    }
}

/**
 * Loop through the alternative responses in the mockdata.
 * And select the first matching response depending on the request
 * parameters, body, headers or cookies.
 * If no match could be found, then return the default response.
 */
function selectResponse(
    res,
    mockdata,
    requestparameters,
    bodyParameters,
    headers,
    cookies
) {
    var mockresponses = mockdata.responses || [];

    for (var i = 0; i < mockresponses.length; i++) {
        var mockresponse = mockresponses[i];
        var parametersMatch =
            !mockresponse.request.parameters ||
            matchParameters(requestparameters, mockresponse.request.parameters);
        var bodyMatch =
            !mockresponse.request.body ||
            matchParameters(bodyParameters, mockresponse.request.body);
        var headersMatch =
            !mockresponse.request.headers ||
            matchParameters(headers, mockresponse.request.headers);
        var cookiesMatch =
            !mockresponse.request.cookies ||
            matchParameters(cookies, mockresponse.request.cookies);

        if (parametersMatch && bodyMatch && headersMatch && cookiesMatch) {
            return mockresponse.response;
        }
    }

    return mockdata.defaultResponse;
}

/**
 * Reqursively compare the incomingParameters with the mockParameters.
 */
function matchParameters(incomingParameters, mockParameters) {
    var keys = Object.getOwnPropertyNames(mockParameters);

    if (!incomingParameters || keys.length === 0) {
        return false;
    }

    for (var i = 0; i < keys.length; i++) {
        var key = keys[i];

        if (typeof mockParameters[key] === "object") {
            //is object, match on next level
            if (
                !matchParameters(incomingParameters[key], mockParameters[key])
            ) {
                return false;
            }
        } else {
            //is primitive
            if (mockParameters[key] !== incomingParameters[key]) {
                return false;
            }
        }
    }

    return true;
}
